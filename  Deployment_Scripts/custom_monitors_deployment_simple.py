from pyzabbix.api import ZabbixAPI, ZabbixAPIException
from datetime import datetime
import mariadb,sys,os, xml.dom.minidom, json, argparse, csv, logging, time, glob,time,re

def getDBindex(database=None,table=None):
    curDBMRS.execute("SELECT ID FROM " + table + " ORDER BY ID DESC LIMIT 1")
    for ID in curDBMRS:
       return ID

### ZABBIX API CONNECTION - VAL ###
#try:
#   zapi = ZabbixAPI(url='https://zabbix-server-val',user='user',password='password')
#   print("Connected to ZABBIX VAL API: Version %s" % zapi.api_version())
#   zabbix_environment = 'VAL'
#   print(zabbix_environment)
#except ZabbixAPIException as e:
#   print(e)

### ZABBIX API CONNECTION - DEV ###
try:
   zapi = ZabbixAPI(url='https://zabbix-server-dev',user='user',password='password')
   print("Connected to ZABBIX DEV API: Version %s" % zapi.api_version())
   zabbix_environment = 'DEV'
   print(zabbix_environment)
except ZabbixAPIException as e:
   print(e)


hostgroups=[]

### ZABBIX API CONNECTION - PROD ###
#try:
#   zapi = ZabbixAPI(url='https://zabbix-server-prod', user='user', password='password')
#   print("Connected to ZABBIX PROD API: Version %s" % zapi.api_version())
#   zabbix_environment = 'PROD'
#except ZabbixAPIException as e:
#   print(e)
### DATABASE CONNECTION ###

try:
   dbMRS="EMRS_DEPLOY"
   connDBMRS = mariadb.connect(
             user="user",
             password="password",
             host="127.0.0.1",
             port=3306,
             database=dbMRS)
   curDBMRS = connDBMRS.cursor()

except mariadb.Error as e:
    print(f"Error connecting to MariaDB Platform: {e}")
    sys.exit(1)

priority = {
        "Lowest": "2",
        "High": "3",
        "Moderate": "3",
        "Priority": "4",
        "": "5"
}

def getHostID(host):
   zhost = zapi.host.get(filter=({'host':host}))
   return zhost[0]['hostid']

def readMRS():
      curDBMRS=connDBMRS.cursor()
      #curDBMRS.execute("""select APP_CODE,lower(FQDN),ARGS,THRESHOLD,MONITOR,SEVERITY,COLLECTOR,EMRS_COUNTRY,EMRS_ENVIRONMENT,EMRS_FULLNAME,IMPACT from DB_MRS WHERE APP_CODe='B879' and wave='2' and del_status='No' """)
#      curDBMRS.execute("""SELECT APP_CODE,lower(FQDN),ARGS,THRESHOLD,MONITOR,SEVERITY,COLLECTOR,EMRS_COUNTRY,EMRS_ENVIRONMENT,EMRS_FULLNAME,IMPACT FROM DB_MRS WHERE wave='6' AND DEL_STATUS='No'""")
      curDBMRS.execute("""SELECT APP_CODE,lower(FQDN),ARGS,THRESHOLD,MONITOR,SEVERITY,COLLECTOR,EMRS_COUNTRY,EMRS_ENVIRONMENT,EMRS_FULLNAME,IMPACT FROM TB_HW_MRS WHERE DEL_STATUS='No' AND MONITOR='Temagent Status' AND FQDN='caefr0p105.eu.airbus.corp'""")
      result = curDBMRS.fetchall()
      return result

def getHostGroups(APP_CODE,host):
   ## get hostgroups from a host
   zhost = zapi.host.get(
           filter=({'host':host}),
           selectGroups='extend' 
        )
   #print("APP_CODE: "+APP_CODE)
   ## get hostid from host
   hostid = getHostID(host)
   wave2group = {'name':'Wave6','groupid':'1878'}
   reviewgroup = {'name':'Wave6_Toreview','groupid':'1879'}
   hostgroups.append(wave2group)
   hostgroups.append(reviewgroup)

   ## init list with hostgroups
   ### for each group in the host
   for i in zhost[0]['groups']:
      ## add in a dictionary {'name':'AB30','groupid':'145099'} (example)
      wave2group = {'name':'Wave6','groupid':'1878'}
      reviewgroup = {'name':'Wave6_Toreview','groupid':'1879'}
      existinggroup = {'name':i['name'],'groupid':i['groupid']}
      ## append the dictionary in the list
      hostgroups.append(existinggroup)
      ## for each hostgroup in the list
      for j in hostgroups:
         ## if APP_CODE is not present in the list
         if APP_CODE != j['name']:
            ## get group id from the group to add
            groupidtoadd = zapi.hostgroup.get(filter={'name':APP_CODE})
            if len(groupidtoadd) == 0:
               try:
                  groupcreated = zapi.hostgroup.create(name=APP_CODE)
               except:
                  print("Impossible to create group "+APP_CODE)
            else:
               ## add dictionary with {'name':'AC30','groupid':'183990'} (example)
               grouptoadd = {'name':APP_CODE,'groupid':groupidtoadd[0]['groupid']}
               ## append new values to hostgroup list
               hostgroups.append(grouptoadd)
               try:
                  ## API Call to update host with new groups
                  addgrp = zapi.host.update(hostid=hostid,groups=hostgroups)
               except:
                  print("problem to add group")
   hostgroups.clear()

# This function use zabbix API to gather all items attached to a host
# it return itemid and itemname
# to collect values, use a for loop with index 

def getItemInZabbix(hostid,itemkey):
   if('.exe' in itemkey):
      itemkey=itemkey.split('.')
      itemkey=itemkey[1].split('[')
      getitemid = zapi.item.get(hostids=hostid,search=({'key_':itemkey}),output="itemid")
      if len(getitemid) != 0:
         zapi.item.delete(
                 getitemid[0]['itemid']
                 )
   elif '.exe' not in itemkey:
      getitemid = zapi.item.get(hostids=hostid,search=({'key_':itemkey}),output="itemid")
      if len(getitemid) != 0:
         print("Item already exist: deletion in progress...")
         zapi.item.delete(
                 getitemid[0]['itemid']
                 )

def triggerCreation(description,expression,priority,tags,dependencies=None):
   try:
      if(dependencies==None):
         result = zapi.trigger.create(description=description,expression=expression,priority=priority,tags=tags)
         return result
      else:
         result = zapi.trigger.create(description=description,expression=expression,priority=priority,tags=tags,dependencies=dependencies)
         return result
   except ZabbixAPIException as e:
      print(e)

def createTrigger(monitor,description1,description2,expression,threshold,tags):
   if ( monitor=='file existence' ):
      #if(i == '100'):
      description = description2 +" on {HOST.HOST}"
      severity='4'
      triggerCreation(description,expression,severity,tags,None)
 
   if ( monitor=='file age' ):
     description = description1 + description2 + " on {HOST.HOST}"
     severity='4'
     triggerCreation(description,expression,severity,tags,None)
   
   if ( monitor =='Cupsd Status' ):
     description = description +" on {HOST.HOST}"
     severity='3'
     triggerCreation(description,expression,severity,tags,None)

   if ( monitor == 'Temagent Status' ):
     description = description1 + " on {HOST.HOST}"
     severity='3'
     triggerCreation(description,expression,severity,tags,None)

   if ( monitor == 'file count' ):
     description = description1 + description2 + " on {HOST.HOST}"
     severity = '4'
     triggerCreation(description,expression,severity,tags,None)

   if ( monitor == 'file size' ):
     description = description1 + description2 + " on {HOST.HOST}"
     severity = '4'
     triggerCreation(description,expression,severity,tags,None)

   if ( monitor == 'FileSystem Mount Status' ):
     description = description1 + description2 + " on {HOST.HOST}"
     severity = '4'
     triggerCreation(description,expression,severity,tags,None)
   
   if ( monitor == 'FileSystem Available Space' ):
     description = description1 + description2 + " on {HOST.HOST}"
     severity = '4'
     triggerCreation(description,expression,severity,tags,None)
   
   if ( monitor == 'Filesystem Writability' ):
     description = description1 + description2 + " on {HOST.HOST}"
     severity = '4'
     triggerCreation(description,expression,severity,tags,None)

   if ( monitor == 'Linux Service Status'):
     description = description1 + description2 + " on {HOST.HOST}"
     severity = '4'
     triggerCreation(description,expression,severity,tags,None)
   
   if ( monitor == 'Ntpd Status'):
     description = description1 + description2 + " on {HOST.HOST}"
     severity = '4'
     triggerCreation(description,expression,severity,tags,None)

   if ( monitor == 'System Uptime'):
     description = description1 + " on {HOST.HOST}"
     severity = '3'
     triggerCreation(description,expression,severity,tags,None)

   if ( monitor=='% Filesystem Space Used' or monitor == 'Free Space' or monitor == 'FSCAPACITY' or monitor=='LDLDFREESPACEPERCENT' ):
     descriptionhigh = description1 + "critically low" + description2 + " "+threshold[0]+"%) on {HOST.HOST}"
     descriptionavg = description1 + "very low" + description2 + " "+threshold[1]+"%) on {HOST.HOST}"
     descriptionwarn = description1 + " low" + description2 + " "+threshold[2]+"%) on {HOST.HOST}"
     expressionhigh = expression+threshold[0]
     expressionavg = expression+threshold[1]
     expressionwarn = expression+threshold[2]
     severityhigh='4'
     severityavg='3'
     severitywarn='2'
     triggerHigh = triggerCreation(descriptionhigh, expressionhigh,severityhigh,tags,None)
     triggerAverage = triggerCreation(descriptionavg, expressionavg,severityavg,tags,[{"triggerid":triggerHigh['triggerids'][0]}])
     triggerWarning = triggerCreation(descriptionwarn, expressionwarn,severitywarn,tags,[{"triggerid":triggerHigh['triggerids'][0]}])

   if ( monitor=='Memory Usage' or monitor =='Real memory available for use by applications' ):
      if(len(threshold)>1):  
          descriptionhigh = description1 + threshold[0] + "%" + description2+" on {HOST.HOST}"
          descriptionavg =  description1 + threshold[1] + "%" + description2+" on {HOST.HOST}"
          expressionhigh = expression+threshold[0]
          expressionavg = expression+threshold[1]
          severityhigh = '4'
          severityavg = '3'
          triggerHigh = triggerCreation(descriptionhigh,expressionhigh,severityhigh,tags,None)
          triggerAvg = triggerCreation(descriptionavg,expressionavg,severityavg,tags,dependencies=[{"triggerid":triggerHigh['triggerids'][0]}])
      else:
         descriptionhigh = description1 +threshold[0]+"%"+description2+" on {HOST.HOST}"
         expression += threshold[0]
         severity=='4'
         triggerhigh = triggerCreation(descriptionhigh,expression,severity,tags,None)
   
   if ( monitor=='Port State' or monitor=='PORTSTATE'):
      severity = '5'
      description1 += " on {HOST.HOST}"
      triggerCreation(description1,expression,severity,tags,None)
 
   if ( monitor == 'Number of processes monitored' or monitor == 'Process Count' or monitor=='Process Status' or monitor == 'PROCPPCOUNT' or monitor == 'PROCCOUNT' or monitor == 'process state' ):
      description1 += " on {HOST.HOST}"
      expressiondst = expression+".max(5m)}<"+threshold[0]
      expressionhigh = expression+".min(5m)}>"+threshold[1]
      severitydst = '5'
      severityhigh = '4'
      triggerDisaster=triggerCreation(description1,expressiondst,severitydst,tags,None)
      triggerHigh=triggerCreation(description2,expressionhigh,severityhigh,tags,[{"triggerid":triggerDisaster['triggerids'][0]}])

   if ( monitor == "Service Status" or monitor == "SERVICESTATUS" ):
      severity = '4'
      description1 += " on {HOST.HOST}"
      triggerCreation(description1,expression,severity,tags,None)

   if ( monitor == 'Sshd Status' ):
      severity = '4'
      description1 += " on {HOST.HOST}"
      triggerCreation(description1,expression,severity,tags,None)

   if ( monitor == 'Total Processor Utilization' ):
      severity = '4'
      descriptionhigh = description1 + threshold[0] + description2 + " on {HOST.HOST}"
      descriptionavg =  description1 + threshold[1] + description2 + " on {HOST.HOST}"
      expressionhigh = expression+threshold[0]
      expressionavg = expression+threshold[1]
      severityhigh = '4'
      severityavg = '3'
      triggerHigh = triggerCreation(descriptionhigh,expressionhigh,severityhigh,tags,None)
      triggerAvg = triggerCreation(descriptionavg,expressionavg,severityavg,tags,[{"triggerid":triggerHigh['triggerids'][0]}])

   if ( monitor == 'sun hw failure' ):
      severity = '4'
      description = description1 + " on {HOST.HOST}"
      triggerCreation(description,expression,severity,tags,None)

   if ( monitor == 'url state'):
      severity = '4'
      description = description1 + " on {HOST.HOST}"
      triggerCreation(description,expression,severity,tags,None)

######

def checkAPPExistence(hostid,item_application):
    ## get all apps with item_application name
    apps = zapi.application.get(hostids = hostid,filter=({'name':item_application}))
    ## if apps not empty
    if apps:
    ## store app id in appz variable
       appz = apps[0]['applicationid']
       appz = [appz]
       return appz
    else: ## if app empty
       apps = zapi.application.create(
                  name = item_application,
                  hostid = hostid #zbxhostid = zhost['hostids']
                  )
       appz = apps['applicationids']
       return appz

def createItemInZabbixWithUnits(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,Units):
   try:
      zhost = zapi.item.create(
                    name = itemname,
                    key_ = itemkey,
                    hostid = hostid,
                    value_type = item_value_type,
                    type = item_type,
                    applications = apps,
                    units = Units,
                    interfaceid = interface[0]['interfaceid'],
                    delay = item_delay)
      deploy_status='OK'

   except ZabbixAPIException as e:
      deploy_status='KO'
      print(e)



def createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,formula=None):
   try:
      if(formula==None):
         zhost = zapi.item.create(
                    name = itemname,
                    key_ = itemkey,
                    hostid = hostid,
                    value_type = item_value_type,
                    type = item_type,
                    applications = apps,
                    interfaceid = interface[0]['interfaceid'],
                    delay = item_delay)
      else:
         zhost = zapi.item.create(
                    name = itemname,
                    key_ = itemkey,
                    hostid = hostid,
                    value_type = item_value_type,
                    type = item_type,
                    params = formula,
                    interfaceid = interface[0]['interfaceid'],
                    delay = item_delay)
      deploy_status='OK'

   except ZabbixAPIException as e:
      deploy_status='KO'
      print(e)


def createMRSItemsinZabbix():
   SQLQuery="INSERT INTO DB_LOGS_SIMPLE(DEPLOY_DATE,DEPLOY_STATUS,BACKUP_STATUS,APP_CODE,HOSTGROUP,MRS_TYPE,FQDN,MONITOR,ARGS,TRIGGER_STATUS,TRIGGER_EXPRESSION,ERROR_MESSAGE,WAVE,ENV) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
   ### INSERT INTO DB_LOGS_SIMPLE ###
   wave='6'
   deploy_date=None
   deploy_status='OK'
   backup_status='tbd'
   mrs_type='APPLICATION'
   trigger_status='tbd'
   trigger_expression='tbd'
   index=None
   for mrs in readMRS():
      hostgroup=mrs[0]
      host=mrs[1]
      args=mrs[2]
      threshold=mrs[3]
      monitor=mrs[4]
      impact=mrs[10]
      if(impact=='X'):
         impact='YES'
      else:
         impact='NO'
      severity=mrs[5]
      EMRS_COUNTRY=mrs[7]
      EMRS_ENVIRONMENT=mrs[8]
      EMRS_FULLNAME=mrs[9]
      APP_CODE=mrs[0]
      hostid=getHostID(host)
      if(monitor=='file existence'):
         itemkey = 'file.existence['+args+']'
         item_delay = '5m'
         item_type="7"
         item_value_type="0"
         item_application='Custom Monitors'
         itemname = 'C-System File Existence'
                        
         description1 = args + " File "+args
         description2 = " does not exist"
         expression = "{" + host + ":" + itemkey + ".last()}=0"
         tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
               {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
               {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
               {'tag':'COUNTRY','value':EMRS_COUNTRY},
               {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
               {'tag':'IMPACT','value':impact},
               {'tag':'SN_MONITOR_NAME','value':itemname}]
         
         ## get all apps with item_application name 
         apps = checkAPPExistence(hostid,item_application)

         interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
         try:
            getItemInZabbix(hostid,itemkey)
            getHostGroups(APP_CODE,host)
            createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,None)
            createTrigger(monitor,description1,description2,expression,threshold,tags)
            error_message=''
            print(hostgroup,host,itemname,deploy_status)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))

         except ZabbixAPIException as e: 
            deploy_status='KO'
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
         try:
            connDBMRS.commit()
         except mariadb.Error as e:
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)
      
      if(monitor=='Cupsd Status'):
         itemkey = 'proc.num[cupsd]'
         item_delay = '5m'
         item_type="7"
         item_value_type="3"
         item_application='Custom Monitors'
         itemname = 'UX Cupsd Status'

         description1 = "Process "+args+" is not running"
         description2 = "Process "+args+" has too many processes"
         expression = "{" + host + ":" + itemkey+".max(5m)}<1"
         tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
               {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
               {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
               {'tag':'COUNTRY','value':EMRS_COUNTRY},
               {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
               {'tag':'IMPACT','value':impact},
               {'tag':'SN_MONITOR_NAME','value':itemname}]

         ## get all apps with item_application name
         apps = checkAPPExistence(hostid,item_application)

         interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
         try:
            getItemInZabbix(hostid,itemkey)
            getHostGroups(APP_CODE,host)
            createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,None)
            createTrigger(monitor,description1,description2,expression,threshold,tags)
            error_message=''
            print(hostgroup,host,itemname,deploy_status)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))

         except ZabbixAPIException as e:
            deploy_status='KO'
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
         try:
            connDBMRS.commit()
         except mariadb.Error as e:
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)
 
      if( monitor == 'Temagent Status' ):
         itemkey = 'proc.num[temagent]'
         item_delay = '5m'
         item_type="7"
         item_value_type="3"
         item_application='Custom Monitors'
         itemname = 'C-UX Temagent Status'

         description1 = "Process Temagent is not running"
         description2 = None
         expression = "{" + host + ":" + itemkey+".max(5m)}<1"
         tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
               {'tag':'IMPACT','value':impact},
               {'tag':'SN_MONITOR_NAME','value':itemname}]

         ## get all apps with item_application name
         apps = checkAPPExistence(hostid,item_application)

         interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
         try:
            getItemInZabbix(hostid,itemkey)
            getHostGroups(APP_CODE,host)
            createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,None)
            createTrigger(monitor,description1,description2,expression,threshold,tags)
            error_message=''
            print(hostgroup,host,itemname,deploy_status)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))

         except ZabbixAPIException as e:
            deploy_status='KO'
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
         try:
            connDBMRS.commit()
         except mariadb.Error as e:
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)



      if(monitor=='file age'):
         itemkey = 'file.age['+args+']'
         item_delay = '5m'
         item_type="7"
         item_value_type="0"
         item_application='Custom Monitors'
         itemname = 'C-System File Age'
         threshold_value = threshold.split(' ')
         threshold_value = threshold_value[1]
         threshold_function = threshold.split(' ')
         threshold_function = threshold_function[0]
         description1 = "Age of file: "+args+" is "
         description2 = threshold
         if(threshold_function=='above'):
            trigger_operator='>'
            trigger_function='min'
         elif(threshold_function=='below'):
            trigger_operator='<'
            trigger_function='max'
         
         expression = "{" + host + ":" + itemkey +'.'+trigger_function+'(15m)}'+trigger_operator + threshold_value
         tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
               {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
               {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
               {'tag':'COUNTRY','value':EMRS_COUNTRY},
               {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
               {'tag':'IMPACT','value':impact},
               {'tag':'SN_MONITOR_NAME','value':itemname}]

         ## get all apps with item_application name
         apps = checkAPPExistence(hostid,item_application)

         interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
         try:
            getItemInZabbix(hostid,itemkey)
            getHostGroups(APP_CODE,host)
            createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,None)
            createTrigger(monitor,description1,description2,expression,threshold,tags)
            error_message=''
            print(hostgroup,host,itemname,deploy_status)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))

         except ZabbixAPIException as e:
            deploy_status='KO'
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
         try:
            connDBMRS.commit()
         except mariadb.Error as e:
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)

      if(monitor=='file count'):
         itemkey = 'file.count['+args+']'
         escaped_args=re.escape(args)
         item_delay = '5m'
         item_type="7"
         item_value_type="3"
         item_application='Custom Monitors'
         itemname = 'System File Count'
         threshold_value = threshold.split(' ')
         threshold_value = threshold_value[1]
         threshold_function = threshold.split(' ')
         threshold_function = threshold_function[0]
         threshold_delay = threshold_value.split(';')
         threshold_delay = threshold_delay[1]
         threshold_max_value = threshold_value.split(';')
         threshold_max_value = threshold_max_value[0]
         print("threshold_value: "+threshold_max_value)
         description1 = "Number of files in : "+args+ " is "+threshold_function
         description2 = threshold_max_value
         if(threshold_function=='above'):
            trigger_operator='>'
            trigger_function='min'
         elif(threshold_function=='below'):
            trigger_operator='<'
            trigger_function='max'

         expression = "{" + host + ":" + itemkey +'.'+trigger_function+'('+threshold_delay+'m)}'+trigger_operator + threshold_max_value
         print(expression)
         tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
               {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
               {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
               {'tag':'COUNTRY','value':EMRS_COUNTRY},
               {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
               {'tag':'IMPACT','value':impact},
               {'tag':'SN_MONITOR_NAME','value':itemname}]

         ## get all apps with item_application name
         apps = checkAPPExistence(hostid,item_application)

         interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
         try:
            getItemInZabbix(hostid,itemkey)
            getHostGroups(APP_CODE,host)
            createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,None)
            createTrigger(monitor,description1,description2,expression,threshold,tags)
            error_message=''
            print(hostgroup,host,itemname,deploy_status)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))

         except ZabbixAPIException as e:
            deploy_status='KO'
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
         try:
            connDBMRS.commit()
         except mariadb.Error as e:
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)

      if(monitor=='file size'):
         itemkey = 'file.size['+args+']'
         escaped_args=re.escape(args)
         item_delay = '5m'
         item_type="7"
         item_value_type="3"
         item_application='Custom Monitors'
         itemname = 'C-System File Size'
         threshold_value = threshold.split(' ')
         threshold_value = threshold_value[1]
         threshold_function = threshold.split(' ')
         threshold_function = threshold_function[0]
         threshold_max_value = threshold_value.split(';')
         threshold_max_value = threshold_max_value[0]
         if(threshold_function=='above'):
            trigger_operator='>'
            trigger_function='min'
         elif(threshold_function=='below'):
            trigger_operator='<'
            trigger_function='max'
         description1 = "Size of "+args+ " file is "+trigger_operator+' '
         description2 = threshold_max_value

         expression = "{" + host + ":" + itemkey +'.'+trigger_function+'(15m)}'+trigger_operator + threshold_max_value
         print(expression)
         tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
               {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
               {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
               {'tag':'COUNTRY','value':EMRS_COUNTRY},
               {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
               {'tag':'IMPACT','value':impact},
               {'tag':'SN_MONITOR_NAME','value':itemname}]

         ## get all apps with item_application name
         apps = checkAPPExistence(hostid,item_application)

         interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
         try:
            getItemInZabbix(hostid,itemkey)
            getHostGroups(APP_CODE,host)
            createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,None)
            createTrigger(monitor,description1,description2,expression,threshold,tags)
            error_message=''
            print(hostgroup,host,itemname,deploy_status)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))

         except ZabbixAPIException as e:
            deploy_status='KO'
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
         try:
            connDBMRS.commit()
         except mariadb.Error as e:
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)

      if(monitor=='FileSystem Mount Status'):
         itemkey = 'mount.check['+args+']'
         escaped_args=re.escape(args)
         item_delay = '2m'
         item_type="7"
         item_value_type="3"
         item_application='Custom Monitors'
         itemname = 'C-UX Filesystem Mount Status'
         description1 = "FileSystem looks not mounted properly on "
         description2 = args

         expression = "{" + host + ":" + itemkey +'.min(15m)}<>1'
         print(expression)
         tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
               {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
               {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
               {'tag':'COUNTRY','value':EMRS_COUNTRY},
               {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
               {'tag':'IMPACT','value':impact},
               {'tag':'SN_MONITOR_NAME','value':itemname}]

         ## get all apps with item_application name
         apps = checkAPPExistence(hostid,item_application)

         interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
         try:
            getItemInZabbix(hostid,itemkey)
            getHostGroups(APP_CODE,host)
            createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,None)
            createTrigger(monitor,description1,description2,expression,threshold,tags)
            error_message=''
            print(hostgroup,host,itemname,deploy_status)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))

         except ZabbixAPIException as e:
            deploy_status='KO'
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
         try:
            connDBMRS.commit()
         except mariadb.Error as e:
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)

      if(monitor=='Filesystem Writability'):
         itemkey = 'filesystem.writability['+args+']'
         escaped_args=re.escape(args)
         item_delay = '5m'
         item_type="7"
         item_value_type="3"
         item_application='Custom Monitors'
         itemname = 'C-UX Filesystem Writability'
         description1 = "FileSystem "+args
         description2 = " is not writable"

         expression = "{" + host + ":" + itemkey +'.min(5m)}<>0'
         print(expression)
         tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
               {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
               {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
               {'tag':'COUNTRY','value':EMRS_COUNTRY},
               {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
               {'tag':'IMPACT','value':impact},
               {'tag':'SN_MONITOR_NAME','value':itemname}]

         ## get all apps with item_application name
         apps = checkAPPExistence(hostid,item_application)

         interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
         try:
            getItemInZabbix(hostid,itemkey)
            getHostGroups(APP_CODE,host)
            createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,None)
            createTrigger(monitor,description1,description2,expression,threshold,tags)
            error_message=''
            print(hostgroup,host,itemname,deploy_status)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))

         except ZabbixAPIException as e:
            deploy_status='KO'
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
         try:
            connDBMRS.commit()
         except mariadb.Error as e:
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)

      if(monitor=='Linux Service Status'):
         itemkey = 'service.status['+args+']'
         item_delay = '2m'
         item_type="7"
         item_value_type="3"
         item_application='Custom Monitors'
         itemname = 'C-UX Linux Service Status'
         description1 = "Service "+args
         description2 = " is not running"

         expression = "{" + host + ":" + itemkey +'.min(5m)}>0'
         print(expression)
         tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
               {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
               {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
               {'tag':'COUNTRY','value':EMRS_COUNTRY},
               {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
               {'tag':'IMPACT','value':impact},
               {'tag':'SN_MONITOR_NAME','value':itemname}]

         ## get all apps with item_application name
         apps = checkAPPExistence(hostid,item_application)

         interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
         try:
            getItemInZabbix(hostid,itemkey)
            getHostGroups(APP_CODE,host)
            createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,None)
            createTrigger(monitor,description1,description2,expression,threshold,tags)
            error_message=''
            print(hostgroup,host,itemname,deploy_status)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))

         except ZabbixAPIException as e:
            deploy_status='KO'
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
         try:
            connDBMRS.commit()
         except mariadb.Error as e:
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)

      if(monitor=='Ntpd Status'):
         itemkey = 'ntpd.service.status'
         item_delay = '5m'
         item_type="7"
         item_value_type="3"
         item_application='Custom Monitors'
         itemname = 'UX Ntpd Status'
         description1 = "Service ntpd"
         description2 = " is not running"

         expression = "{" + host + ":" + itemkey +'.min(5m)}>0'
         print(expression)
         tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
               {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
               {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
               {'tag':'COUNTRY','value':EMRS_COUNTRY},
               {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
               {'tag':'IMPACT','value':impact},
               {'tag':'SN_MONITOR_NAME','value':itemname}]

         ## get all apps with item_application name
         apps = checkAPPExistence(hostid,item_application)

         interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
         try:
            getItemInZabbix(hostid,itemkey)
            getHostGroups(APP_CODE,host)
            createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,None)
            createTrigger(monitor,description1,description2,expression,threshold,tags)
            error_message=''
            print(hostgroup,host,itemname,deploy_status)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))

         except ZabbixAPIException as e:
            deploy_status='KO'
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)
            curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
         try:
            connDBMRS.commit()
         except mariadb.Error as e:
            error_message=str(e)
            print(hostgroup,host,itemname,error_message)


      if(monitor=='% Filesystem Space Used' or monitor == 'Free Space' or monitor == 'FSCAPACITY' or monitor=='LDLDFREESPACEPERCENT'):
            itemkey = 'vfs.fs.size['+args+',pused]'
            if threshold != '80.0':
               threshold = threshold.split(' ')
            else:
               threshold = ['100','90','80']
            item_delay = '5m'
            item_type="7"
            item_value_type="0"
            item_application='Filesystems'
            itemname = 'System % Filesystem Space Used'
            
            description1 = args + " Disk space is "
            description2 = " (used > "
            expression = "{" + host + ":" + itemkey + ".min(15m)}>"
            tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
                  {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
                  {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
                  {'tag':'COUNTRY','value':EMRS_COUNTRY},
                  {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
                  {'tag':'IMPACT','value':impact},
                  {'tag':'SN_MONITOR_NAME','value':itemname}]
            
            ## get all apps with item_application name 
            apps = checkAPPExistence(hostid,item_application)

            interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
            try:
               getItemInZabbix(hostid,itemkey)
               getHostGroups(APP_CODE,host)
               createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,None)
               createTrigger(monitor,description1,description2,expression,threshold,tags)
               error_message=''
               print(hostgroup,host,itemname,deploy_status)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
 
            except ZabbixAPIException as e:
               deploy_status='KO'
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            try:
               connDBMRS.commit()
            except mariadb.Error as e:
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)

      if(monitor=='FileSystem Available Space'):
            itemkey = 'vfs.fs.size['+args+',free]'
            item_delay = '2m'
            item_type="7"
            item_value_type="0"
            item_application='Filesystems'
            itemname = 'Filesystem Available Space'
            units = 'M'
            description1 = "Free space on "+args
            description2 = " < "+threshold
            expression = "{" + host + ":" + itemkey + ".min(15m)}<"+threshold
            tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
                  {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
                  {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
                  {'tag':'COUNTRY','value':EMRS_COUNTRY},
                  {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
                  {'tag':'IMPACT','value':impact},
                  {'tag':'SN_MONITOR_NAME','value':itemname}]

            ## get all apps with item_application name
            apps = checkAPPExistence(hostid,item_application)

            interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
            try:
               getItemInZabbix(hostid,itemkey)
               getHostGroups(APP_CODE,host)
#               createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,None)
               createItemInZabbixWithUnits(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,units)
               createTrigger(monitor,description1,description2,expression,threshold,tags)
               error_message=''
               print(hostgroup,host,itemname,deploy_status)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))

            except ZabbixAPIException as e:
               deploy_status='KO'
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            try:
               connDBMRS.commit()
            except mariadb.Error as e:
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)




      if(monitor=='Memory Usage'):
            itemkey1 = "vm.memory.size[total]"
            itemkey2 = "vm.memory.size[used]"
            itemkey3 = "vm.memory.util"
            itemname1 = "Total Memory"
            itemname2 = "Used Memory"
            itemname3 = "System Memory Usage"
            threshold = threshold.split(';')
            if(threshold!=None):
               thresholdtime=threshold[1]
            else:
               thresholdtime="15"
            threshold = threshold[0].split(' ')
            item_delay='2m'
            item_application='Memory'
            description1 = "High memory utilization ( > "
            description2 = " for "+thresholdtime+"m)"
            tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
                  {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
                  {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
                  {'tag':'COUNTRY','value':EMRS_COUNTRY},
                  {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
                  {'tag':'IMPACT','value':impact},
                  {'tag':'SN_MONITOR_NAME','value':itemname3}]

            apps = checkAPPExistence(hostid,item_application)

            interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
            try:
               item_type="7"
               unit='B'
               item_value_type="3"
               expression = "{" + host + ":" + itemkey1 + ".min(15m)}>"
               getItemInZabbix(hostid,itemkey1)
               getHostGroups(APP_CODE,host)
               createItemInZabbix(itemname1,itemkey1,hostid,item_value_type,item_type,apps,item_delay,interface)
               error_message=''
               deploy_status='OK'
               print(hostgroup,host,itemname1,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment)) 
            except ZabbixAPIException as e: 
               deploy_status='KO'
               error_message=str(e)
               print(hostgroup,host,itemname1,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            try:
               item_type="7"
               unit='B'
               item_value_type="3"
               expression = "{" + host + ":" + itemkey2 + ".min(15m)}>"
               getItemInZabbix(hostid,itemkey2)
               createItemInZabbix(itemname2,itemkey2,hostid,item_value_type,item_type,apps,item_delay,interface)
               error_message=''
               deploy_status='OK'
               print(hostgroup,host,itemname2,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            except ZabbixAPIException as e:
               deploy_status='KO'
               error_message=str(e)
               print(hostgroup,host,itemname2,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            try:
               item_type='15'
               item_value_type='0'
               unit='%'
               formula='last("vm.memory.size[used]") / last("vm.memory.size[total]") * 100'
               expression = "{" + host + ":" + itemkey3 + ".min(15m)}>"
               getItemInZabbix(hostid,itemkey3)
               createItemInZabbix(itemname3,itemkey3,hostid,item_value_type,item_type,apps,item_delay,interface,formula)
               createTrigger(monitor,description1,description2,expression,threshold,tags)
               error_message=''
               deploy_status='OK'
               print(hostgroup,host,itemname3,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            except ZabbixAPIException as e:
               deploy_status='KO'
               error_message=str(e)
               print(hostgroup,host,itemname3,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            try:
               connDBMRS.commit()
            except mariadb.Error as e:
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)


      if monitor == "Port State" or monitor == "PORTSTATE":
            itemkey="net.tcp.service[tcp,,"+args+"]"
            item_status="0"
            itemname= 'System Port State'
            item_type="3"
            item_value_type="3"
            item_application="Port"
            item_delay="5m"
            item_valuemapcreate='1'
            description1 = "Port "+args+" is down"
            description2 = None
            expression = "{" + host + ":" + itemkey + ".min(5m)}<>1"
            tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
                  {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
                  {'tag': 'EMRS_FULLNAME','value':EMRS_FULLNAME},
                  {'tag': 'COUNTRY','value':EMRS_COUNTRY},
                  {'tag': 'ENVIRONMENT','value':EMRS_ENVIRONMENT},
                  {'tag':'IMPACT','value':impact},
                  {'tag': 'SN_MONITOR_NAME','value':itemname}]

            ## get all apps with item_application name
            apps = checkAPPExistence(hostid,item_application)

            interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
            try:
               getHostGroups(APP_CODE,host)
               getItemInZabbix(hostid,itemkey)
               createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface,None)
               createTrigger(monitor,description1,description2,expression,threshold,tags)
               error_message=''
               deploy_status='OK'
               print(hostgroup,host,itemname,deploy_status)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            except ZabbixAPIException as e:
               deploy_status='KO'
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            try:
               connDBMRS.commit()
            except mariadb.Error as e:
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)

      if monitor == 'Number of processes monitored' or monitor == 'Process Count' or monitor=='Process Status' or monitor == 'PROCPPCOUNT' or monitor == 'PROCCOUNT' or monitor == 'process state':
            collectorlist=[]
            collector = mrs[6]
            threshold = mrs[3].split(";")
            collectorlist.append(collector)
            if "abc-windows" in collectorlist:
               args = threshold[0]+".exe"
               itemkey="proc.num["+args+"]"
            else:
               args = threshold[0]
               itemkey="proc.num["+args+"]"
            threshold = threshold[1].split(" ")            
            item_status="0"
            itemname= 'System Process Count'
            item_type="7"
            item_value_type="3"
            item_application="Processes"
            item_delay="5m"
            item_valuemapcreate='0'
            description1 = "Process "+args+" is not running"
            description2 = "Process "+args+" has too many processes"
            expression = "{" + host + ":" + itemkey 
            tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
                  {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
                  {'tag': 'EMRS_FULLNAME','value':EMRS_FULLNAME},
                  {'tag': 'COUNTRY','value':EMRS_COUNTRY},
                  {'tag': 'ENVIRONMENT','value':EMRS_ENVIRONMENT},
                  {'tag':'IMPACT','value':impact},
                  {'tag': 'SN_MONITOR_NAME','value':itemname}]
            ## get all apps with item_application name
            apps = checkAPPExistence(hostid,item_application)
            interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
            try:
               getHostGroups(APP_CODE,host)
               getItemInZabbix(hostid,itemkey)
               createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface)
               createTrigger(monitor,description1,description2,expression,threshold,tags)
               error_message=''
               deploy_status='OK'
               print(hostgroup,host,itemname,deploy_status)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            except ZabbixAPIException as e:
               deploy_status='KO'
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            try:
               connDBMRS.commit()
            except mariadb.Error as e:
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)

      if ( monitor == "Service Status" or monitor == "SERVICESTATUS" ):
            itemkey="service.info["+args+",state]"
            item_status="0"
            itemname='System Service Status'
            item_type="7"
            item_value_type="3"
            item_application="Services"
            item_delay="5m"
            item_valuemapcreate="1"

            description1 = "Service "+args+" is not running"
            description2 = None
            expression = "{" + host + ":" + itemkey + ".min(5m)}<>0"
            tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
                  {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
                  {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
                  {'tag':'COUNTRY','value':EMRS_COUNTRY},
                  {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
                  {'tag':'IMPACT','value':impact},
                  {'tag':'SN_MONITOR_NAME','value':itemname}]

            ## get all apps with item_application name
            apps = checkAPPExistence(hostid,item_application)
            interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
            try:
               getHostGroups(APP_CODE,host)
               getItemInZabbix(hostid,itemkey)
               createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface)
               createTrigger(monitor,description1,description2,expression,threshold,tags)
               error_message=''
               deploy_status='OK'
               print(hostgroup,host,itemname,deploy_status)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            except ZabbixAPIException as e:
               deploy_status='KO'
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            try:
               connDBMRS.commit()
            except mariadb.Error as e:
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)

      if ( monitor == "Sshd Status" ):
            itemkey="sshd.status"
            item_status="0"
            itemname='UX Sshd Status'
            item_type="7"
            item_value_type="3"
            item_application="Services"
            item_delay="5m"
            item_valuemapcreate="1"

            description1 = "Service Sshd is not running"
            description2 = None
            expression = "{" + host + ":" + itemkey + ".min(5m)}<>0"
            tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
                  {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
                  {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
                  {'tag':'COUNTRY','value':EMRS_COUNTRY},
                  {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
                  {'tag':'IMPACT','value':impact},
                  {'tag':'SN_MONITOR_NAME','value':itemname}]

            ## get all apps with item_application name
            apps = checkAPPExistence(hostid,item_application)
            interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
            try:
               getHostGroups(APP_CODE,host)
               getItemInZabbix(hostid,itemkey)
               createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface)
               createTrigger(monitor,description1,description2,expression,threshold,tags)
               error_message=''
               deploy_status='OK'
               print(hostgroup,host,itemname,deploy_status)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            except ZabbixAPIException as e:
               deploy_status='KO'
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            try:
               connDBMRS.commit()
            except mariadb.Error as e:
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)

      if ( monitor == "sun hw failure" ):
            itemkey="sun.hw.failure"
            item_status="0"
            itemname='UX Sshd Status'
            item_type="7"
            item_value_type="4"
            item_application="Hardware Failures"
            item_delay="10m"
            item_valuemapcreate="1"

            description1 = "Hardware failures detected"
            description2 = None
            expression = "{" + host + ":" + itemkey + ".strlen()}>0"
            tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
                  {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
                  {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
                  {'tag':'COUNTRY','value':EMRS_COUNTRY},
                  {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
                  {'tag':'IMPACT','value':impact},
                  {'tag':'SN_MONITOR_NAME','value':itemname}]

            ## get all apps with item_application name
            apps = checkAPPExistence(hostid,item_application)
            interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
            try:
               getHostGroups(APP_CODE,host)
               getItemInZabbix(hostid,itemkey)
               createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface)
               createTrigger(monitor,description1,description2,expression,threshold,tags)
               error_message=''
               deploy_status='OK'
               print(hostgroup,host,itemname,deploy_status)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            except ZabbixAPIException as e:
               deploy_status='KO'
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            try:
               connDBMRS.commit()
            except mariadb.Error as e:
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)
      
      if ( monitor == "System Uptime" ):
            itemkey="system.uptime"
            item_status="0"
            itemname='UX System Uptime'
            item_type="7"
            item_value_type="4"
            item_application="Custom Monitors"
            item_delay="1m"
            item_valuemapcreate="1"
            description1 = "System Uptime > "+threshold+" seconds"
            description2 = None
            expression = "{" + host + ":" + itemkey + ".last()}>"+threshold
            tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
                  {'tag':'IMPACT','value':impact},
                  {'tag':'SN_MONITOR_NAME','value':itemname}]

            ## get all apps with item_application name
            apps = checkAPPExistence(hostid,item_application)
            interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
            try:
               getHostGroups(APP_CODE,host)
               getItemInZabbix(hostid,itemkey)
               createItemInZabbix(itemname,itemkey,hostid,item_value_type,item_type,apps,item_delay,interface)
               createTrigger(monitor,description1,description2,expression,threshold,tags)
               error_message=''
               deploy_status='OK'
               print(hostgroup,host,itemname,deploy_status)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            except ZabbixAPIException as e:
               deploy_status='KO'
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            try:
               connDBMRS.commit()
            except mariadb.Error as e:
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)


      if monitor == "Total Processor Utilization":
            threshold = mrs[3].split(";")
            threshold = threshold[0]
            thresholdtime = threshold[1]
            if thresholdtime==None or thresholdtime=='0':
               thresholdtime='15'
            threshold = threshold.split(" ")
            itemkey="system.cpu.util"
            item_status="0"
            itemname='System Total Processor Utilization'
            item_type="7"
            item_value_type="0"
            item_application="CPU"
            item_delay="2m"
            item_valuemapcreate="0"

            description1 = "High CPU utilization (over "
            description2 = "% for "+thresholdtime+"m)"
            expression = "{" + host + ":system.cpu.util.min(" + thresholdtime + "m)}>"
            tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
                  {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
                  {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
                  {'tag':'COUNTRY','value':EMRS_COUNTRY},
                  {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
                  {'tag':'IMPACT','value':impact},
                  {'tag':'SN_MONITOR_NAME','value':monitor}]

            ## get all apps with item_application name
            apps = checkAPPExistence(hostid,item_application)
            interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
            try:
               getHostGroups(APP_CODE,host)
               getItemInZabbix(hostid,itemkey)
               zhost = zapi.item.create(
                     name = itemname,
                     key_ = itemkey,
                     hostid = hostid,
                     value_type = item_value_type,
                     type = item_type,
                     units = "%",
                     applications = apps,
                     interfaceid = interface[0]['interfaceid'],
                     delay = item_delay)
               createTrigger(monitor,description1,description2,expression,threshold,tags)
               error_message=''
               deploy_status='OK'
               print(hostgroup,host,itemname,deploy_status,zabbix_environment)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))

            except ZabbixAPIException as e:
               deploy_status='KO'
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            try:
               connDBMRS.commit()
            except mariadb.Error as e:
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)

      if monitor == "url state":
            itemkey="http.check["+args+"]"
            URL = args
            item_status="0"
            itemname="C-System Url State"
            item_type="19"
            item_value_type="0"
            item_application="WEB"
            item_delay="5m"
            item_valuemapcreate="0"

            description1 = "HTTP Response code not valid"
            description2 = None
            expression = "{" + host + ":"+itemkey+".last()}<>200 and {"+host+":"+itemkey+".last()}<>201 and {"+host+":"+itemkey+".last()}<>302"
            tags=[{'tag': 'TYPE', 'value': 'SYSTEM'},
                  {'tag': 'APPLICATION_CODE', 'value': APP_CODE},
                  {'tag':'EMRS_FULLNAME','value':EMRS_FULLNAME},
                  {'tag':'COUNTRY','value':EMRS_COUNTRY},
                  {'tag':'ENVIRONMENT','value':EMRS_ENVIRONMENT},
                  {'tag':'IMPACT','value':impact},
                  {'tag':'SN_MONITOR_NAME','value':itemname}]
            ## get all apps with item_application name
            apps = checkAPPExistence(hostid,item_application)
            interface = zapi.hostinterface.get(output='extend',hostids=hostid,filter=({'type':1}))
            try:
               getHostGroups(APP_CODE,host)
               getItemInZabbix(hostid,itemkey)
               regex = R"^HTTP.*\s+(\d+).*\\n\\1"
               zhost = zapi.item.create(
                    name = itemname,
                    key_ = itemkey,
                    hostid = hostid,
                    url=URL,
                    retrieve_mode= '1',
                    value_type = item_value_type,
                    type = item_type,
                    applications = apps,
                    interfaceid = interface[0]['interfaceid'],
                    delay = item_delay,
                    preprocessing=
                           [{'type': '5', 'params': '^HTTP.*\\s+(\\d+).*\n\\1', 'error_handler': '0', 'error_handler_params': ''}]                 
                       )
               createTrigger(monitor,description1,description2,expression,threshold,tags)
               error_message=''
               deploy_status='OK'
               print(hostgroup,host,itemname,deploy_status,zabbix_environment)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))

            except ZabbixAPIException as e:
               deploy_status='KO'
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)
               curDBMRS.execute(SQLQuery,(deploy_date,deploy_status,backup_status,hostgroup,hostgroup,mrs_type,host,monitor,args,trigger_status,trigger_expression,error_message,wave,zabbix_environment))
            try:
               connDBMRS.commit()
            except mariadb.Error as e:
               error_message=str(e)
               print(hostgroup,host,itemname,error_message)
 

createMRSItemsinZabbix()

#print(readMRS())