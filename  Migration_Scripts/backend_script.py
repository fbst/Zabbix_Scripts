#from openpyxl import load_workbook
import csv,glob,argparse,xlrd,sys,warnings,os,threading
import pandas as pd
import logging
if(sys.version_info.major == 3):
   import mariadb
elif(sys.version_info.major == 2):
   import mysql.connector

### Arguments processing ###
path = sys.argv[1]
mrstype = sys.argv[2]
convert = sys.argv[3]
dbupdate = sys.argv[4]
table = sys.argv[5]

#### Definition of the output folder considering the MRS TYPE provided 
#### in the parameters of the script call
if mrstype == 'application':
   output_path = "/root/python_scripts/Migration-scripts/Conversion_scripts/output_MRS/output_application/"
elif mrstype == 'hardware':
   output_path = "/root/python_scripts/Migration-scripts/Conversion_scripts/output_MRS/output_hardware/"
elif mrstype == 'database':
   output_path = "/root/python_scripts/Migration-scripts/Conversion_scripts/output_MRS/output_database/"



### Initiate log file ###

#logging.basicConfig(level=logging.DEBUG,filename="conversion.log",filemode='a',format='%(asctime)s - %(levelname)s - '+mrstype+' - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
#logger=logging.getLogger()
formatter = logging.Formatter('%(asctime)s - %(levelname)s - '+mrstype+' - %(message)s',datefmt='%d-%b-%y %H:%M:%S')

def setup_logger(name, log_file, level=logging.INFO):
    handler = logging.FileHandler(log_file)        
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger

conversion_logger = setup_logger('CONVERSION_LOGGER','/root/python_scripts/Migration-scripts/Conversion_scripts/logs/conversion_import.log')
db_logger = setup_logger('DB_LOGGER','/root/python_scripts/Migration-scripts/Conversion_scripts/logs/db_import.log')
### Disable warnings for openpyxl ###

warnings.filterwarnings('ignore', category=UserWarning, module='openpyxl')


### Variables declarations ###
if ".csv" not in path:
   csv_filename = path.split('/')
   csv_filename = csv_filename[7]
   csv_filename = csv_filename.split('.')
   csv_filename = csv_filename[0]
   csv_filename = csv_filename.rsplit('_',1)
   csv_filename = csv_filename[0]
   csv_full_filename = output_path+csv_filename+'.csv'
else:
   csv_full_filename = path
#path = args.inputfile
lines = []
excel_file_lines = []
monitorList = []
conversion_execution_success = False
stdMonitors = ['% Filesystem Space Used','LDLDFREESPACEPERCENT','Ping Status','Service Status','Free Space','url state','Port State','Number of processes monitored','Process Count','PROCPPCOUNT','LOGFILE','HOSTPINGFAILURES','% CPU Utilization','Real memory available for use by applications','Total Processor Utilization','Memory Usage','FSCAPACITY','PORTSTATE','SERVICESTATUS','file count','PROCCOUNT','HOSTPINGFAILIURES','HOSPINGFAILURES','Process Status','HOSTPINGFAILURE','process state']

### Oracle Monitors list ###
oracleMonitors = ['LISTENERLOG','LISTERNERSTATUS','INSTANCESTATUS','SPACELEFT','PCTUSED','CANNOTEXTEND','EXTENTSLEFTPCT','EXTENTSLEFTETF','ENQDEADLOCKS','PROCUSED','SESSIONUSED','FRAPCTUSED','ARCHIVELOGUSAGE','Apply Lag','archive log','Archived Log Files Percentage Used','Cannot Auto Extend','Extents Left %','Flash Recovery Area Space Used in Percentage','Free Space Available','Instance Status','Listener Status','Observer Status','oracle alert log','Processes Used','rdbms state','Sessions Used','Space Allocated','Standby Status','tabspacenxt','tbsnbtrou','tbssznxt','tbsusedspace']

### Oracle Exadata Monitors List ###
oracleExadataMonitors = ['Container Database Status','Data Guard Status','Filesystem Space Available (%)','Host Availability','Listener Availability','OEM Agent Availability','OEM Observer Status','Pluggable Database Status','Recovery Area Used Space (%)','Tablespace Space Used (%)','CRS Node App','Disk Free Space %','DiskGroupStatus','Oracle Automatic Storage Management Instance Status','Real Application Cluster Instances Member Status']

### PostgresSQL Monitors List ###
postgresMonitors = ['Connections Percentage Used','Database Availability','Instance Availability','Patroni Leader Count','Postgres Etcd Cluster Health Status']

### MSSQL Monitors List ###
mssqlMonitors = ['Database Status','Listener State','Mirroring State','Role','SQL Agent Service Status','SQL Connection Status','sql event log','SQL Server Service Status','Synchronization Health']



### DATABASE CONNECTION ###

try:
   ### This script can be called from python3 or python2. 
   ### To connect to mariadb, the syntax is not the same
   ### if you are using python3 or python2. This is why 
   ### this python version is checked.
   if(sys.version_info.major == 3):
      import mariadb
      dbMRS="EMRS_DEPLOY"
      connDBMRS = mariadb.connect(
             user="mariadb_user",
             password="mariadb_password",
             host="127.0.0.1",
             port=3306,
             database=dbMRS)
      curDBMRS = connDBMRS.cursor()
   
   elif(sys.version_info.major == 2):
      import mysql.connector
      dbMRS="EMRS_DEPLOY"
      connDBMRS = mysql.connector.connect(
             user="mariadb_user",
             password="mariadb_password",
             host="127.0.0.1",
             port=3306,
             database=dbMRS)
      curDBMRS = connDBMRS.cursor()

except mariadb.Error as e:
    print("Error connecting to MariaDB Platform: ",e)
    sys.exit(1)


### Functions ###

def readMRS():
      SQLQuery = "SELECT fqdn,monitor FROM "+table+" WHERE MRS_TYPE='"+mrstype+"'"
      curDBMRS.execute(SQLQuery)
      result = curDBMRS.fetchall()
      return result

##########################################################################
### This function is used to get the last id of the DB
##########################################################################
def getDBindex():
    curDBMRS.execute("SELECT ID FROM " + table + " ORDER BY ID DESC LIMIT 1")
    for ID in curDBMRS:
       return ID

##########################################################################
### in this function the script will check if the table given into 
### parameters exist or not. It yes, we are returning 1. Else we are 
### returning 0
##########################################################################

def checkTableExistence(table):
   tableList=[]
   curDBMRS.execute("use EMRS_DEPLOY")
   curDBMRS.execute("SHOW TABLES")
   tables = curDBMRS.fetchall()
   for i in tables:
      tableList.append(i[0])
   if table in tableList:
      return 1
   else:
      return 0

#########################################################################
### In this function, we will read the CSV File given into parameters.
### in the for loop, each variable used between [] befor the "in"
### keyword are fiting exactly with the columns present in the csv file.
### We are processing some of them and we are returning a list of all 
### the data gathered in the csv file
#########################################################################

def readMRSFile(csv_file):
      csvfile = open(csv_file,"r")
      csvfile = csv.reader(csvfile,delimiter=',')
      for [fapplication_name,fservice,fservice_type,fperiod,fintermediate_service,fweight,fhost,fargs,fthreshold,fimpact,fmonitor,fnetwork,fsolution,fverification,fh24,fseverity,fcollector,fremedy,fimpacted_service,fopteam,fstatus] in csvfile:
         if mrstype == 'application' or mrstype == 'APPLICATION':
            filename = csv_file.split("/")
            filename = filename[7].split('.csv')
            full_name_splitted= filename[0].split('_')
            appcode = full_name_splitted[0]
            #TEST_TEST_FRA_PRODUCTION_v1.0t7_requirements_sheet_119979.csv
            if len(full_name_splitted) > 3:
               EMRS_FULLNAME_part1 = full_name_splitted[1]
               EMRS_FULLNAME_part2 = full_name_splitted[2]
               EMRS_FULLNAME_part3 = full_name_splitted[3]
               EMRS_FULLNAME = EMRS_FULLNAME_part1+'_'+EMRS_FULLNAME_part2+'_'+EMRS_FULLNAME_part3
               EMRS_COUNTRY = full_name_splitted[2]
               EMRS_ENVIRONMENT = full_name_splitted[3]
            else:
               EMRS_COUNTRY = "UNKNOWN"
               EMRS_ENVIRONMENT = "UNKNOWN"
               EMRS_FULLNAME = appcode+'_'+EMRS_COUNTRY+'_'+EMRS_ENVIRONMENT
         else:
            appcode = 'UNKNOWN'
            EMRS_FULLNAME = 'UNKNOWN'
            EMRS_COUNTRY = 'UNKNOWN'
            EMRS_ENVIRONMENT = 'UNKNOWN'
         yield fapplication_name,fservice,fservice_type,fperiod,fintermediate_service,fweight,fhost,fargs,fthreshold,fimpact,fmonitor,fnetwork,fsolution,fverification,fh24,fseverity,fcollector,fremedy,fimpacted_service,fopteam,fstatus,appcode,EMRS_FULLNAME,EMRS_COUNTRY,EMRS_ENVIRONMENT


###################################################################
### This function will regroup different call of several functions
### it will first use the checkTableExistence with the table into 
### parameters to verify that the table used exist before trying 
### to insert data.
### Next, the script will call the readMRSFile function to read 
### the csv file and gather all the datas. You will have more
### details on the comment of the function.
### Next we are setting all the variables regarding what is set 
### in the csv file.
### Finaly, the SQLQuery variable is filled with the entire
### query and is executed agains the database.
###################################################################

def fillDBWithMRS(table):
   if checkTableExistence(table) == 1:
      print("Inserting data into "+table+" table...")
      for i in readMRSFile(csv_full_filename):
         index = getDBindex()
         if index is None:
            index = 0
         else:
            index = str(index).split(',')
            index = index[0].split('(')
            index = index[1]
         index                = int(index)+1
         application_name     = i[0]
         service              = i[1]
         service_type         = i[2]
         restriction_time     = i[3]
         intermediate_service = i[4]
         weight               = i[5]
         fqdn                 = i[6]
         shortname            = i[6].split('.')
         shortname            = shortname[0]
         args                 = i[7]
         threshold            = i[8]
         impact               = i[9]
         monitor              = i[10]
         network              = i[11]
         solution             = i[12]
         verification         = i[13]
         H24                  = i[14]
         severity             = i[15]
         collector            = i[16]
         remedy               = i[17]
         impacted_service     = i[18]
         opteam               = i[19]
         status               = i[20]
         appcode              = i[21]
         EMRS_FULLNAME        = i[22]
         EMRS_COUNTRY         = i[23]
         EMRS_ENVIRONMENT     = i[24]
         if(monitor in stdMonitors):
            standard = 'yes'
         else:
            standard = 'No'
         monitorList.append(monitor)
         SQLQuery = "INSERT INTO "+table+"(id,APP_CODE,MRS_TYPE,SHORTNAME,APPLICATION_NAME,SERVICE,SERVICE_TYPE,RESTRICT_TIME,INTERMEDIATE_SERVICE,WEIGHT,FQDN,ARGS,THRESHOLD,IMPACT,MONITOR,NETWORK,SOLUTION,VERIFICATION,H24,SEVERITY,COLLECTOR,REMEDY,IMPACTED_SERVICE,OPTEAM,DEL_STATUS,STANDARD,EMRS_COUNTRY,EMRS_ENVIRONMENT,EMRS_FULLNAME) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
         try:
            db_logger.info("FQDN: %s - MONITOR: %s - ARGS: %s - THRESHOLD: %s - TABLE: %s",fqdn,monitor,args,threshold,table)
            if len(monitor)>0:
               curDBMRS.execute(SQLQuery,(index,appcode,mrstype,shortname,application_name,service,service_type,restriction_time,intermediate_service,weight,fqdn,args,threshold,impact,monitor,network,solution,verification,H24,severity,collector,remedy,impacted_service,opteam,status,standard,EMRS_COUNTRY,EMRS_ENVIRONMENT,EMRS_FULLNAME))
               connDBMRS.commit()
            else:
               db_logger.error("FQDN: %s - MONITOR IS EMPTY",fqdn)
         except mariadb.Error as e:
            db_logger.error("Error while importing monitor %s in %s -> %s ",monitor,table,e)
      monlist = set(monitorList)
      mssqllist = set(mssqlMonitors)
      oraclelist = set(oracleMonitors)
      oracleexadatalist = set(oracleExadataMonitors)
      postgreslist = set(postgresMonitors)
      if(mrstype=='database'):
         if(monlist & mssqllist):
            db_type="MSSQL"
            SQLQuery = "UPDATE "+table+" SET DB_TYPE='"+db_type+"' WHERE fqdn='"+fqdn+"'"
            curDBMRS.execute(SQLQuery)
            connDBMRS.commit()
         elif(monlist & oraclelist):
            db_type="Oracle"
            SQLQuery = "UPDATE "+table+" SET DB_TYPE='"+db_type+"' WHERE fqdn='"+fqdn+"'"
            curDBMRS.execute(SQLQuery)
            connDBMRS.commit()
         elif(monlist & oracleexadatalist):
            db_type="Oracle Exadata"
            SQLQuery = "UPDATE "+table+" SET DB_TYPE='"+db_type+"' WHERE fqdn='"+fqdn+"'"
            curDBMRS.execute(SQLQuery)
            connDBMRS.commit()
         elif(monlist & postgreslist):
            db_type="PostgreSQL"
            SQLQuery = "UPDATE "+table+" SET DB_TYPE='"+db_type+"' WHERE fqdn='"+fqdn+"'"
            curDBMRS.execute(SQLQuery)
            connDBMRS.commit()
   else:
      print("Table "+table+" does not exist")

#######################################################
### This function will convert a dataframe passed into
### parameters. The output will be a csv file who will
### be placed in the output folder specified
#######################################################
 
def generateCSV(dataframe,final_csv_file):
   try:
      ### this will permit in the case of merged cells
      ### in the excel files. It will duplicate the 
      ### value. For example for the column C, you
      ### have 3 lines merged with one value. Theses
      ### line will permit to have 3 lines unmerged 
      ### in the csv and each line will have the value

      dataframe[0].fillna(method='ffill',inplace=True)
      dataframe[1].fillna(method='ffill',inplace=True)
      dataframe[2].fillna(method='ffill',inplace=True)
      dataframe[3].fillna(method='ffill',inplace=True)
      dataframe[4].fillna(method='ffill',inplace=True)
      dataframe[5].fillna(method='ffill',inplace=True)
      dataframe[6].fillna(method='ffill',inplace=True)
      ### Call of the to_csv pandas function. It will 
      ### generate the final CSV file
      dataframe.to_csv(final_csv_file,index=None,header=None,encoding='UTF8',sep=',')
      print(final_csv_file+" file converted.")
   except Exception as e:
      print(e)

######################################################
### The convertWithPandas function will permet to 
### convert an excel file to a CSV File
######################################################

def convertWithPandas(excel_file,csv_full_filename):
      ### We are checking the version of python used, because the openpyxl
      ### library work only with python3 file and we need this library 
      ### to convert xlsx files, you'll have more details in the run_convertion.py script
      if(sys.version_info.major == 3): ### If python3 is used ###
         try:
            df = pd.read_excel(excel_file,engine='openpyxl',sheet_name='MetaApplicationView',skiprows=2,header=None,index_col=None,na_values=['N/A'],usecols=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20])
            generateCSV(df,csv_full_filename)
            conversion_logger.info('%s file converted',excel_file)
            conversion_execution_success = True
         except:
            conversion_logger.error('%s ERROR while converting file ',excel_file)
            conversion_execution_success = False
 
      ### In case of python2 version used, we will use the default library of pandas
      elif(sys.version_info.major == 2): ### If python 2 is used ###
         try:
            ### In the dataframe (df) declaration, we are targeting one excel file and one specific sheet (MetaApplicationView)
            ###  we are skipping the 2 first rows of the excel file (these are the name of the columns), and we are taking the 20 columns of the file
            df = pd.read_excel(excel_file,sheetname='MetaApplicationView',skiprows=2,header=None,index_col=None,na_values=['N/A'],usecols=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20])
            generateCSV(df,csv_full_filename)
            conversion_logger.info('%s file converted',excel_file)
            conversion_execution_success = True
         except:
            conversion_logger.error('%s ERROR while converting file ',excel_file)
            conversion_execution_success = False
      else:
         conversion_logger.error('invalid python version while importing %s',excel_file)
         conversion_execution_success = False

def getDbMonitorsForIdentification():
   SQLQuery = "SELECT fqdn,monitor FROM "+table+" WHERE MRS_TYPE='"+mrstype+"'"
   curDBMRS.execute(SQLQuery)
   result = curDBMRS.fetchall()
   return result
   

### MAIN ###

def main():
   ### check if the --convert parameter is used in the call of the script
   ### if not, the script won't convert anything
   
   if convert == "1":
      convertWithPandas(path,csv_full_filename)
   
   ### check if the table variable, dbupdate and truncate are set.
   ### if yes, the table will be truncated before the import
   
   if len(table)>0 and dbupdate == "1":
      fillDBWithMRS(table)
   else:
      print("The table name cannot be empty")

if __name__ == '__main__':
   main()
   connDBMRS.close()