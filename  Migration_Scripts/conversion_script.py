import os,glob,sys,argparse,time,subprocess

#### Parsing of the arguments ####

parser = argparse.ArgumentParser()
parser.add_argument('--mrstype', required=False, help='The MRS Type (application,hardware,database), must be lower case')
parser.add_argument('--inputfolder', required=False, help='The input folder where MRS file are stored')
parser.add_argument('--convert', required=False,nargs='?',default=argparse.SUPPRESS,help='If this option is selected, the script will try to convert all xlsx or xls found in the --inpufolder path and to convert to csv file')
parser.add_argument('--dbupdate', required=False,nargs='?',default=argparse.SUPPRESS,help='If this option is selected, the table on EMRS_DEPLOY DB on tooling server will be updated')
parser.add_argument('--table', required=False,default=argparse.SUPPRESS,help='The name of the table to be updated with the informations found in CSV files, the script will check if this table exist in EMRS_DEPLOY and will fail if the table does not exist')

args,unknown = parser.parse_known_args()


### Check if the strings is present
### in the parameters of the script call
### if yes return 1 if not return 0
 
if 'dbupdate' in args:
   dbupdate = "1"
else:
   dbupdate = "0"

if 'table' in args:
   table = args.table
else:
   table = "Not defined" 

if 'convert' in args:
   convert = "1"
else:
   convert = "0"
   
path = args.inputfolder
mrstype = args.mrstype
start_time = time.time()
xlsxFileList=[]
xlsFileList=[]
testList=[]
mrstypes = ['application','hardware','database']

########################################################################################
### This function worker will explore the folder passed into parameters 
### and execute the read.py script with python3 or 2 regarding the extension of the file
### if xlsx we will use python3.6 else if is xls file we will use python2 
########################################################################################

def worker(path,mrstype,convert,dbupdate,table):
      for files in sorted(glob.iglob(path+'*.*')): 
         print(files)
         extension = files.rsplit('.',1)
         extension = extension[1]
         filename = files.split("/")
         filename = filename[5]
         if(extension=='xlsx'):
            subprocess.call(["python3.6","backend_script.py",files,mrstype,convert,dbupdate,table])
         elif(extension=='xls'):
            subprocess.call(["python","backend_script.py",files,mrstype,convert,dbupdate,table])

def clean(mrstype):
   files = glob.glob('/root/python_scripts/Migration-scripts/Conversion_scripts/output_MRS/output_'+mrstype+'/*')
   for file_to_clean in files:
      os.remove(file_to_clean)

#### Function Call and timer display ###

worker(path,mrstype,convert,dbupdate,table)
clean(mrstype)
print("--- %s seconds ---" % (time.time() - start_time))