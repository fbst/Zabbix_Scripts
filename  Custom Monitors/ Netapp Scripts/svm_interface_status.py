#!/usr/bin/env python3.6

import csv,os,subprocess,sys,re,json,socket

def getIps(hostname):
   svm_ips=[]
   ips=socket.gethostbyname_ex(hostname)
   for ip in ips[2]:
      svm_ips.append(ip)
   return svm_ips

def readHostFile():
   HostListFilePath="/usr/local/zabbix/externalscripts/svm_host_list.csv"
   HostListFile=csv.reader(open(HostListFilePath))
   HostList=[]
   for [host] in HostListFile:
      HostList.append(host)
   return HostList

def testMountedPoint():
    host=sys.argv[1]
    for ip in getIps(host): 
        cmd = 'showmount -e '+ip+' --no-headers | grep "^\/\s"'
        output = subprocess.getoutput(cmd)
        if len(output)>0:
           status='OK'
 #          print(" # -> IP: "+ip+" result: "+status)
           yield host,ip,status
        else:
           status='KO'
#           print(" # -> IP: "+ip+" result: "+status)
           yield host,ip,status

def generateJsonOutput():
   json_output=[]
   for i in testMountedPoint():
      Parent=[]
      Values={}
      Values["SVM"]=i[1]
      Values["{#SVM.IP}"]=i[1]
      Values["SVM.STATUS"]=i[2]
      json_output.append(Values)
   return json_output

json_object = json.dumps(generateJsonOutput(),indent=4)
print(json_object)
