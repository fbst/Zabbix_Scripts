#!/usr/bin/env python3.6

import sys, json,re,subprocess

#Values={}
#json_output=[]
host = sys.argv[1]
user = sys.argv[2]
password = sys.argv[3]

TargetConfiguration = "sshpass -p \'"+password+"\' ssh -q -o 'StrictHostKeyChecking no' \""+user+"@"+host+"\" 'set -priv diag ;vscan scanner-pool servers show -scanner-pool primary'"
ActiveConfiguration = "sshpass -p \'"+password+"\' ssh -q -o 'StrictHostKeyChecking no' \""+user+"@"+host+"\" 'set -priv diag ;vserver vscan connection-status show'"

with open("/tmp/running_config.txt","wt") as outputfile:
   result = subprocess.getoutput(TargetConfiguration)
   print(result)
   if "/bin/sh:" not in result and len(result)>0:
      data = outputfile.write(result)
with open("/tmp/config.txt","wt") as outputfile:
   result = subprocess.getoutput(ActiveConfiguration)
   print(result)
   if "/bin/sh:" not in result and len(result)>0:
      data = outputfile.write(result)



def readFile():
    file = open('/tmp/config.txt','r')
    files = file.readlines()
    output=[]
    for line in files:
        regexp = re.findall(r'^\w+-svm.*',line)
        for i in regexp:
            output.append(i)
    return output

def readConfigurationFile():
    file = open('/tmp/running_config.txt','r')
    files = file.readlines()
    output=[]
    for line in files:
        regexp = re.findall(r'^\w+-svm.*',line)
        for i in regexp:
            output.append(i)
    return output

def getSVMConfiguration(host):
    output=[]
    for line in readConfigurationFile():
        if host in line:
            output.append(line)
    return output

def getSVM(host):
    output=[]
    for line in readFile():
        if host in line:
            output.append(line)
    return output

def getNode(host):
    Node=[]
    for line in getSVM(host):
        regexp = re.search(r'(?=\S*[-])([a-zA-Z0-9-]+)\s+(?=\S*[-])([a-zA-Z0-9-]+)\s+(\d)\s(.*)',line)
        Node.append(regexp.group(2))
    return Node

def getSVMHost():
    Hosts = []
    SVMHost=[]
    for line in readFile():
        regexp = re.search(r'(?=\S*[-])([a-zA-Z0-9-]+)\s+(?=\S*[-])([a-zA-Z0-9-]+)\s+(\d)\s(.*)',line)
        SVMHost.append(regexp.group(1))
        Hosts=list(set(SVMHost))
    return Hosts

def NumberofRunningVSCAN(host):
    RunningVscanCount=[]
    for line in getSVM(host):
        regexp = re.search(r'(?=\S*[-])([a-zA-Z0-9-]+)\s+(?=\S*[-])([a-zA-Z0-9-]+)\s+(\d)\s(.*)',line)
        RunningVscanCount.append(regexp.group(3))
    return RunningVscanCount

def getVscanIps(host):
    VscanIps=[]
    for line in getSVM(host):
        regexp = re.search(r'(?=\S*[-])([a-zA-Z0-9-]+)\s+(?=\S*[-])([a-zA-Z0-9-]+)\s+(\d)\s(.*)',line)
        VscanIps.append(regexp.group(4))
    return VscanIps

def getConfigurationScannerPool(host):
    ScannerPool = []
    for line in getSVMConfiguration(host):
        regexp = re.search(r'([a-zA-Z0-9-.]+)\s+([a-z]+)\s+([0-9., ]+)',line)
        ScannerPool.append(regexp.group(2))
    return ScannerPool

def getConfigurationServers(host):
    Servers = []
    for line in getSVMConfiguration(host):
        regexp = re.search(r'([a-zA-Z0-9-.]+)\s+([a-z]+)\s+([0-9., ]+)',line)
        Servers.append(regexp.group(3))
    return Servers

def getCountOfConfiguredServers(host):
    Servers = []
    FinalServerList = []
    ServerList = []
    for line in getSVMConfiguration(host):
        regexp = re.search(r'([a-zA-Z0-9-.]+)\s+([a-z]+)\s+([0-9., ]+)',line)
        Servers.append(regexp.group(3))
        for i in Servers:
           ServerList=i.split(', ')
           for j in range(0,len(ServerList)):
              FinalServerList.append(ServerList[j])
    return len(FinalServerList)

def generateOutput():
    json_output=[]
#    Values = {}
    for i in range(0,len(getSVMHost())):
       Values={}
       #print(getSVMHost()[i])
       Values['Server']=getSVMHost()[i]
       Values['{#VSERVER}']=getSVMHost()[i]
#       Values['Current_Node}']=getNode(getSVMHost()[i])
       Values['Current_Connected_Server_Count']=NumberofRunningVSCAN(getSVMHost()[i])
#       Values['{Current_Connected_Servers}']=getVscanIps(getSVMHost()[i])
#       Values['{Configured_Scanner_Pool}']=getConfigurationScannerPool(getSVMHost()[i])
#       Values['{Configured_Servers}']=getConfigurationServers(getSVMHost()[i])
       Values["Configured_Servers_Count"]=getCountOfConfiguredServers(getSVMHost()[i])
       #for hosts in getSVMHost():
       #   host = hosts
       json_output.append(Values)
    yield json_output 

#print(getCountOfConfiguredServers("fr0-svm10"))
#for i in range(0,len(getSVMHost())):
   #print(getSVMHost()[i])


for i  in generateOutput():
   json_object = json.dumps(i,indent=4)
   print(json_object)



#with open('/home/fab/Documents/GITHUB/Scripts/VSCAN/result.json', 'w', encoding='utf-8') as f:
#    f.write(json_object)
