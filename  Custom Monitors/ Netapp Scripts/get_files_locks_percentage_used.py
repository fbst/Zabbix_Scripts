#!/usr/bin/env python3.6

import subprocess,sys,json,collections,re,random

Files = []
Files_limit = []
Locks = []
Locks_limit = []
json_output = [] 
json_output2 = {}
#Values = {}
Instances = []
host = sys.argv[1]
user = sys.argv[2]
passwd = sys.argv[3]
cmd = "sshpass -p "+passwd+" ssh -q -o 'StrictHostKeyChecking no' "+user+"@"+host+" 'set -priv diag ;statistics show -raw true -object lmgr_ng -instance "+host+"-* -counter files|locks|files_limit|locks_limit'"

with open("/tmp/output.txt","wt") as outputfile:
   data = outputfile.write(subprocess.getoutput(cmd))

def readFile():
    file = open('/tmp/output.txt','r')
    files = file.readlines()
    output=[]
    for line in files:
        output.append(line)
    return output

def getInstances():
    InstanceCount = 0
    Instances = []
    for line in readFile():
        if 'Instance: ' in line:
            Instances.append(line.strip())
            InstanceCount = len(Instances)
            #json_output[Instances]=Instances
    yield InstanceCount,Instances

#def getFiles():
#    name = "files "
#    for line in readFile():
#        if name in line:
#            regexp = re.search(r'\s+\w+\s+(\d+)',line)
#            Files.append(regexp.group(1))
#    return Files

def getFiles():
    for line in readFile():
        name = "files "
        if name in line:
            regexp = re.search(r'\s+\w+\s+(\d+)',line)
            Files.append(regexp.group(1))
    yield Files,name


#def getFilesLimit():
#    name = "files_limit "
#    for line in readFile():
#        if name in line:
#            regexp = re.search(r'\s+\w+\s+(\d+)',line)
#            Files_limit.append(regexp.group(1))
#    return Files_limit

def getFilesLimit():
    name = "files_limit "
    for line in readFile():
        if name in line:
            regexp = re.search(r'\s+\w+\s+(\d+)',line)
            Files_limit.append(regexp.group(1))
    yield Files_limit,name


#def getLocks():
#    for line in readFile():
#        name = "locks "
#        if name in line:
#            regexp = re.search(r'\s+\w+\s+(\d+)',line)
#            Locks.append(regexp.group(1))
#    return Locks

def getLocks():
    for line in readFile():
        name = "locks "
        if name in line:
            regexp = re.search(r'\s+\w+\s+(\d+)',line)
            Locks.append(regexp.group(1))
    yield Locks,name


#def getLocksLimit():
#    for line in readFile():
#        name = "locks_limit "
#        if name in line:
#            regexp = re.search(r'\s+(\w+)\s+(\d+)',line)
#            Locks_limit.append(regexp.group(1))
#    return Locks_limit

def getLocksLimit():
    for line in readFile():
        name = "locks_limit "
        if name in line:
            regexp = re.search(r'\s+\w+\s+(\d+)',line)
            Locks_limit.append(regexp.group(1))
    yield Locks_limit,name
 
def generateOutput():
    for info in getInstances():
        InstanceCount = int(info[0])
        for i in range(0,InstanceCount):
            Values = {} 
            InstanceName = info[1][i].strip("Instance: ")
            if '.partner' not in InstanceName:
               Values["{#INSTANCENAME}"]=InstanceName
#               Values["{#FILES}"]=getFiles()[i]
               for j in getFiles():
                  Values["{#FILESNAME}"]=j[1]
                  Values["{#FILES}"]=j[0][i]
#               Values["{#FILESLIMIT}"]=getFilesLimit()[i]
               for k in getFilesLimit():
                  Values["{#FILESLIMIT}"]=k[0][i]
                  Values["{#FILESLIMITNAME}"]=k[1]
#               Values["{#LOCKS}"]=getLocks()[i]
               for l in getLocks():
                  Values["{#LOCKS}"]=l[0][i]
                  Values["{#LOCKSNAME}"]=l[1]
               for m in getLocksLimit():
                  Values["{#LOCKSLIMIT}"]=m[0][i]
                  Values["{#LOCKSLIMITNAME}"]=m[1]
               #Values["{#RANDINT}"]=random.randint(0,1000)
#               json_output2[InstanceName] = Values
               json_output.append(Values)
    return json_output

#for json_output in generateOutput():
json_object = json.dumps(generateOutput(),indent=4)
#   result = str(json_output)
#   result = result.replace("'","\"")
    # = output.rfind(",")
print(json_object)
